import React from "react";
import styledComponents from "styled-components";
import { gsap } from "gsap";
import Swiper, { Navigation, Pagination } from "swiper";
import 'swiper/css';
import { useEffect, useState } from "react";
// import mainArrowLeft from "../img/mainArrowLeft";
// import mainArrowRight from "../img/mainArrowRight";

const Container = styledComponents.div`
    width: 100%;
    max-width: 1440px;
    height: 100%;
    min-height: 1080px;
    box-sizing: border-box;
    padding-top: 170px;
    margin: 0 auto;
    border-left: 1px solid rgba(66, 86, 122, 0.1);
    border-right: 1px solid rgba(66, 86, 122, 0.1);
    position: relative;
    &::before {
        content: "";
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 1px;
        height: 100%;
        background-color: rgba(66, 86, 122, 0.1);
    }
    &::after {
        content: "";
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        height: 1px;
        background-color: rgba(66, 86, 122, 0.1);
    }
    @media screen and (max-width: 1460px) {
        max-width: 1240px;
    }
    @media screen and (max-width: 1260px) {
        max-width: 1040px;
    }
    @media screen and (max-width: 1060px) {
        max-width: 840px;
    }
    @media screen and (max-width: 860px) {
        max-width: 640px;
    }
    @media screen and (max-width: 660px) {
        max-width: 440px;
        border-left: none;
        border-right: none;
        padding-top: 56px;
        min-height: unset;
        &::before {
            content: none;
        }
        &::after {
            content: none;
        }
    }
    @media screen and (max-width: 460px) {
        max-width: 300px;
    }
`;

const Headline = styledComponents.h1`
    font-family: 'PT Sans Bold', sans-serif;
    font-style: normal;
    font-weight: 700;
    font-size: 56px;
    line-height: 120%;
    color: #42567A;
    padding-left: 80px;
    position: relative;
    margin-bottom: 393px;
    &::before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 5px;
        background: linear-gradient(180deg, #3877EE -5%, #EF5DA8 85%);
    }
    @media screen and (max-width: 660px) {
        font-size: 20px;
        padding-left: 0;
        margin-bottom: 56px;
        &::before {
            content: none;
        }
    }
`;

const Circle = styledComponents.div`
    width: 530px;
    height: 530px;
    border-radius: 50%;
    border: 1px solid rgba(66, 86, 122, 0.2);
    position: absolute;
    z-index: 2;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    & #active {
        background: #F4F5F9;
        border: none;
        transform: scale(9.3);
        &::after {
            opacity: 1;
            visibility: visible;
        }
        & p {
            opacity: 1;
            visibility: visible;
        }
    }
    & button:first-of-type {
        top: 6.2%;
        right: 23.96%; 
        left: unset;
        &::after {
            content: "Test1";
        }
    }
    & button:nth-of-type(2) {
        top: 44.33%;
        right: -0.4%; 
        left: unset;
        &::after {
            content: "Test2";
        }
    }
    & button:nth-of-type(3) {
        top: 92.33%;
        right: 23.96%; 
        left: unset;
        &::after {
            content: "Test3";
        }
    }
    & button:nth-of-type(4) {
        top: 92.33%;
        left: 23.96%;
        &::after {
            content: "Test4";
        }
    }
    & button:nth-of-type(5) {
        top: 44.33%;
        left: -0.4%;
        &::after {
            content: "Test5";
        }
    }
    & button:nth-of-type(6) {
        top: 6.2%;
        left: 23.96%;
        &::after {
            content: "Test6";
        }
    }
    @media screen and (max-width: 660px) {
        display: none;
    }
`;

const Button = styledComponents.button`
    position: absolute;
    z-index: 2;
    top: 0;
    left: 0;
    width: 6px;
    height: 6px;
    background: #42567A;
    border-radius: 50%;
    cursor: pointer;
    transition: all .3s linear;
    &::before {
        content: "";
        position: absolute;
        left: 50%;
        right: 50%;
        transform: translate(-50%, -50%) scale(0.107);
        border: 1px solid rgba(48, 62, 88, 0.5);
        width: 56px;
        height: 56px;
        border-radius: 50%;
        
    }
    &::after {
        content: "";
        position: absolute;
        top: 50%;
        left: -12px;
        transform: translateY(-50%) scale(0.107);
        font-family: 'PT Sans Bold', sans-serif;
        font-weight: 700;
        font-size: 20px;
        line-height: 30px;
        color: #42567A;
        opacity: 0;
        visibility: hidden;
    }
    & p {
        opacity: 0;
        font-family: 'PT Sans Regular', sans-serif;
        font-size: 20px;
        line-height: 30px;
        color: #42567A;
        visibility: hidden;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) scale(0.107);
    }
    &:hover {
        background: #F4F5F9;
        border: none;
        transform: scale(9.3);
        & p {
            opacity: 1;
            visibility: visible;
        }
    }
`;

const Years = styledComponents.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: flex;
    justify-content: space-between;
    width: 973px;
    & h1 {
        font-family: 'PT Sans Bold';
        font-weight: 700;
        font-size: 200px;
        line-height: 160px;
        letter-spacing: -0.02em;
        color: #5D5FEF;
    }
    & .lastYear {
        color: #ef5da8;
    }
    @media screen and (max-width: 1060px) {
        width: 773px;
        & h1 {
            font-size: 150px;
        }
    }
    @media screen and (max-width: 860px) {
        width: 573px;
        & h1 {
            font-size: 100px;
        }
    }
    @media screen and (max-width: 660px) {
        width: 440px;
        margin: 0 auto;
        margin-bottom: 36px;
        & h1 {
            font-size: 56px;
            line-height: 72px;
        }
        position: inherit;
        top: auto;
        left: auto;
        transform: none;
    }
    @media screen and (max-width: 1060px) {
        width: 300px;
    }
`;

const Theme = styledComponents.div`
    display: none;
    border-bottom: 1px solid #C7CDD9;
    padding-bottom: 20px;
    margin-bottom: 20px;
    & .theme {
        font-family: 'PT Sans Bold',sans-serif;
        font-style: normal;
        font-weight: 700;
        font-size: 18px;
        line-height: 120%;
        color: #42567A;
    }
    @media screen and (max-width: 660px) {
        display: block;
    }
`

const MainSliderContainer = styledComponents.div`
    width: 100%;
    max-width: 1360px;
    margin: 0 auto;
    overflow: hidden;
    @media screen and (max-width: 1460px) {
        max-width: 1160px;
    }
    @media screen and (max-width: 1260px) {
        max-width: 960px;
    }
    @media screen and (max-width: 1060px) {
        max-width: 760px;
    }
    @media screen and (max-width: 860px) {
        max-width: 560px;
    }
    @media screen and (max-width: 660px) {
        max-width: 440px;
        display: flex;
        flex-direction: column-reverse;
    }
`;

const MainSliderContainerHead = styledComponents.div`
    width: 100%;
    max-width: 1280px;
    margin: 0 auto;
    margin-bottom: 56px;
    position: relative;
    @media screen and (max-width: 1460px) {
        max-width: 1080px;
    }
    @media screen and (max-width: 1260px) {
        max-width: 880px;
    }
    @media screen and (max-width: 1060px) {
        max-width: 680px;
    }
    @media screen and (max-width: 860px) {
        max-width: 480px;
    }
    @media screen and (max-width: 660px) {
        max-width: 440px;
    }
`;

const MainSliderContainerHeadPagination = styledComponents.div`
    margin-bottom: 20px;
    font-family: 'PT Sans Regular';
    font-weight: 400;
    font-size: 14px;
    line-height: 18px;
    color: #42567A;
    @media screen and (max-width: 660px) {
        margin-bottom: 10px;
    }
`;

const MainSliderContainerHeadBtns = styledComponents.div`
    display: flex;
`;

const MainSliderContainerHeadBtnsButton = styledComponents.button`
    background: none;
    cursor: pointer;
    width: 50px;
    height: 50px;
    border: 1px solid rgba(66, 86, 122, 0.5);
    transform: matrix(-1, 0, 0, 1, 0, 0);
    border-radius: 50%;
    &:last-of-type {
        margin-left: 20px;
    }
    @media screen and (max-width: 660px) {
        width: 25px;
        height: 25px;
        &:last-of-type {
            margin-left: 8px;
        }
        & svg {
            width: 6px;
        }
    }
`;

const MainSliderContainerHeadCircles = styledComponents.div`
    width: 86px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: none;
    & #active {
        opacity: 1;
    }
    @media screen and (max-width: 660px) {
        display: flex;
        justify-content: space-between;
    }
`;

const MainSliderContainerHeadCirclesBtn = styledComponents.button`
    background: #42567A;
    opacity: 0.4;
    cursor: pointer;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    border: none;
`

const MainSliderContainerBody = styledComponents.div`
    width: 100%;
    max-width: 1360px;
    margin: 0 auto;
    @media screen and (max-width: 1460px) {
        max-width: 1160px;
    }
    @media screen and (max-width: 660px) {
        margin-bottom: 78px;
    }
`;

const MainSliderContainerBodySlider = styledComponents.div`
    width: 100%;
    max-width: 1360px;
    margin: 0 auto;
    overflow: hidden;
    display: flex;
    justify-content: space-between;
    align-items: center;
    & .swiper-button-disabled {
        visibility: hidden;
    }
    & .dopBtn-left svg {
        transform: rotate(180deg);
    }
    @media screen and (max-width: 1460px) {
        max-width: 1160px;
    }
    @media screen and (max-width: 1260px) {
        max-width: 960px;
    }
    @media screen and (max-width: 1060px) {
        max-width: 760px;
    }
    @media screen and (max-width: 860px) {
        max-width: 560px;
    }
    @media screen and (max-width: 660px) {
        max-width: 440px;
    }
`;

const MainSliderContainerBodySliderBtn = styledComponents.button`
    width: 40px;
    height: 40px;
    background: #FFFFFF;
    box-shadow: 0px 0px 15px rgba(56, 119, 238, 0.1);
    border-radius: 50%;
    position: relative;
    z-index: 10;
    cursor: pointer;
    border: none;
    @media screen and (max-width: 660px) {
        display: none;
    }
`

const MainSliderContainerBodySliderContainer = styledComponents.div`
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    @media screen and (max-width: 1460px) {
        max-width: 1000px;
    }
    @media screen and (max-width: 1260px) {
        max-width: 800px;
    }
    @media screen and (max-width: 1060px) {
        max-width: 600px;
    }
    @media screen and (max-width: 860px) {
        max-width: 400px;
    }
    @media screen and (max-width: 660px) {
        max-width: 440px;
    }
`;

const MainSliderContainerBodySliderContainerSlide = styledComponents.div`

`;

const MainSliderContainerBodySliderContainerSlideHeadline = styledComponents.h1`
    font-family: 'Bebas Neue', sans-serif;
    font-weight: 400;
    font-size: 25px;
    line-height: 120%;
    text-transform: uppercase;
    color: #3877EE;
    margin-bottom: 15px;
    @media screen and (max-width: 660px) {
        font-size: 16px;
    }
`;

const MainSliderContainerBodySliderContainerSlideText = styledComponents.p`
    font-family: 'PT Sans Regular', sans-serif;
    font-weight: 400;
    font-size: 20px;
    line-height: 30px;
    color: #42567A;
    @media screen and (max-width: 660px) {
        font-size: 14px;
    }
`;

const App = () => {
    const [rotate, setRotate] = useState<number>(60);

    useEffect(() => {
        const mainSwiper = new Swiper(".mainSlider", {
            pagination: {
              el: ".swiper-pagination",
              type: "fraction"
            },
            navigation: {
              nextEl: ".swiper-button-right",
              prevEl: ".swiper-button-left"
            },
            modules: [Navigation, Pagination],
            allowTouchMove: false
        });

        const buttons: any = document.querySelectorAll("#circle>button");
        const firstYear: any = document.querySelector(".firstYear");
        const lastYear: any = document.querySelector(".lastYear");
        const theme: any = document.querySelector(".theme");
        const circlesContainer: any = document.querySelectorAll("#circlesContainer>button");

        for (let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", (e: any) => {
                mainSwiper.slideTo(buttons[i].dataset.index);
                buttons[i].id = "active";

                gsap.to("#circle", {
                    rotate: `${rotate * buttons[i].dataset.x}px`
                })

                firstYear.textContent = buttons[i].dataset.yearstart;
                lastYear.textContent = buttons[i].dataset.yearend;

                console.log(mainSwiper.realIndex);
                

                for (let j = 0; j < buttons.length; j++) {
                    if (buttons[i].dataset.index !== buttons[j].dataset.index && buttons[j].getAttribute("id") === "active") {
                        buttons[j].id = "";
                    }
                }
            })
        }

        for (let i = 0; i < circlesContainer.length; i++) {
            circlesContainer[i].addEventListener("click", (e: any) => {
                mainSwiper.slideTo(circlesContainer[i].dataset.index);
                circlesContainer[i].id = "active";

                firstYear.textContent = circlesContainer[i].dataset.yearstart;
                lastYear.textContent = circlesContainer[i].dataset.yearend;
                theme.textContent = circlesContainer[i].dataset.theme;

                console.log(mainSwiper.realIndex);

                for (let j = 0; j < circlesContainer.length; j++) {
                    if (circlesContainer[i].dataset.index !== circlesContainer[j].dataset.index && circlesContainer[j].getAttribute("id") === "active") {
                        circlesContainer[j].id = "";
                    }
                }
            })
        }

        const leftBtn: any = document.querySelector(".swiper-button-left");
        const rightBtn: any = document.querySelector(".swiper-button-right");

        function changeCircle() {
            for (let i = 0; i < buttons.length; i++) {
                if (mainSwiper.realIndex == buttons[i].dataset.index) {
                    buttons[i].id = "active";
    
                    gsap.to("#circle", {
                        rotate: `${rotate * buttons[i].dataset.x}px`
                    })
        
                    firstYear.textContent = buttons[i].dataset.yearstart;
                    lastYear.textContent = buttons[i].dataset.yearend;
                    theme.textContent = buttons[i].dataset.theme;

                    console.log(mainSwiper.realIndex);

                    for (let j = 0; j < buttons.length; j++) {
                        if (buttons[i].dataset.index !== buttons[j].dataset.index && buttons[j].getAttribute("id") === "active") {
                            buttons[j].id = "";
                        }
                    }
                }
            }
        }

        leftBtn.addEventListener("click", changeCircle);
        rightBtn.addEventListener("click", changeCircle);

        const dopSwiper = new Swiper(".dopSlider", {
            spaceBetween: 80,
            navigation: {
                nextEl: ".dopBtn-right",
                prevEl: ".dopBtn-left"
            },
            modules: [Navigation],
            breakpoints: {
                661: {
                    slidesPerView: 3,
                },
                660: {
                    slidesPerView: 2,
                }
            }
        })
    });

    return (
        <Container>
            <Headline>
                Исторические <br />
                даты
            </Headline>

            <Circle id="circle">
                <Button data-index="0" data-x="1" data-yearstart="1992" data-yearend="1996" data-theme="Test1" id="active">
                    <p>1</p>
                </Button>

                <Button data-index="1" data-x="2" data-yearstart="1997" data-yearend="2001" data-theme="Test2">
                    <p>2</p>
                </Button>

                <Button data-index="2" data-x="3" data-yearstart="2002" data-yearend="2006" data-theme="Test3">
                    <p>3</p>
                </Button>

                <Button data-index="3" data-x="4" data-yearstart="2007" data-yearend="2011" data-theme="Test4">
                    <p>4</p>
                </Button>

                <Button data-index="4" data-x="5" data-yearstart="2012" data-yearend="2016" data-theme="Test5">
                    <p>5</p>
                </Button>

                <Button data-index="5" data-x="6" data-yearstart="2017" data-yearend="2021" data-theme="Test6">
                    <p>6</p>
                </Button>
            </Circle>

            <Years>
                <h1 className="firstYear">1992</h1>
                <h1 className="lastYear">1996</h1>
            </Years>

            <Theme>
                <h1 className="theme">Test1</h1>
            </Theme>

            <MainSliderContainer className="mainSlider">
                <MainSliderContainerHead>
                    <MainSliderContainerHeadPagination className="swiper-pagination"></MainSliderContainerHeadPagination>

                    <MainSliderContainerHeadBtns>
                        <MainSliderContainerHeadBtnsButton className="swiper-button-left">
                            <svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.50012 0.750001L7.75012 7L1.50012 13.25" stroke="#42567A" stroke-width="2"/>
                            </svg> 
                        </MainSliderContainerHeadBtnsButton>

                        <MainSliderContainerHeadBtnsButton className="swiper-button-right">
                            <svg width="10" height="14" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.49988 0.750001L2.24988 7L8.49988 13.25" stroke="#42567A" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerHeadBtnsButton>
                    </MainSliderContainerHeadBtns>

                    <MainSliderContainerHeadCircles id="circlesContainer">
                        <MainSliderContainerHeadCirclesBtn id="active" data-index="0" data-yearstart="1992" data-yearend="1996" data-theme="Test1"></MainSliderContainerHeadCirclesBtn>
                        <MainSliderContainerHeadCirclesBtn data-index="1" data-yearstart="1997" data-yearend="2001" data-theme="Test2"></MainSliderContainerHeadCirclesBtn>
                        <MainSliderContainerHeadCirclesBtn data-index="2" data-yearstart="2002" data-yearend="2006" data-theme="Test3"></MainSliderContainerHeadCirclesBtn>
                        <MainSliderContainerHeadCirclesBtn data-index="3" data-yearstart="2007" data-yearend="2011" data-theme="Test4"></MainSliderContainerHeadCirclesBtn>
                        <MainSliderContainerHeadCirclesBtn data-index="4" data-yearstart="2012" data-yearend="2016" data-theme="Test5"></MainSliderContainerHeadCirclesBtn>
                        <MainSliderContainerHeadCirclesBtn data-index="5" data-yearstart="2017" data-yearend="2021" data-theme="Test6"></MainSliderContainerHeadCirclesBtn>
                    </MainSliderContainerHeadCircles>
                </MainSliderContainerHead>

                <MainSliderContainerBody className="swiper-wrapper">
                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1992
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1993
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1994
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1995
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1996
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>

                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1997
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1998
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    1999
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2000
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2001
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>

                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2002
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2003
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2004
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2005
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2006
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>

                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2007
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2008
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2009
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2010
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2011
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>

                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2012
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2013
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2014
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2015
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2016
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>

                    <MainSliderContainerBodySlider className="dopSlider swiper-slide">
                        <MainSliderContainerBodySliderBtn className="dopBtn-left">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>

                        <MainSliderContainerBodySliderContainer className="swiper-wrapper">
                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2017
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2018
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2019
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2020
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>

                            <MainSliderContainerBodySliderContainerSlide className="swiper-slide">
                                <MainSliderContainerBodySliderContainerSlideHeadline>
                                    2021
                                </MainSliderContainerBodySliderContainerSlideHeadline>

                                <MainSliderContainerBodySliderContainerSlideText>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                </MainSliderContainerBodySliderContainerSlideText>
                            </MainSliderContainerBodySliderContainerSlide>
                        </MainSliderContainerBodySliderContainer>

                        <MainSliderContainerBodySliderBtn className="dopBtn-right">
                            <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L6 6L1 11" stroke="#3877EE" stroke-width="2"/>
                            </svg>
                        </MainSliderContainerBodySliderBtn>
                    </MainSliderContainerBodySlider>
                </MainSliderContainerBody>
            </MainSliderContainer>
        </Container>
    )
}

export default App;