import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/App';
import { createGlobalStyle } from 'styled-components';
import PtSansRegular from "./fonts/PTSans-Regular.ttf";
import PtSansBold from "./fonts/PTSans-Bold.ttf";
import bebasneueregular from "./fonts/bebasneueregular.ttf";

const Global = createGlobalStyle`
  @font-face {
    font-family: 'PT Sans Bold';
    font-weight: 700;
    src: url(${PtSansBold});
  }

  @font-face {
    font-family: 'PT Sans Regular';
    font-weight: 400;
    src: url(${PtSansRegular});
  }

  @font-face {
    font-family: 'Bebas Neue';
    font-weight: 400;
    src: url(${bebasneueregular});
  }

  * {
    margin: 0;
    padding: 0;
  }

  body {
    background: #f4f5f9;
  }

  a {
    text-decoration: none;
  }
`

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <>
    <App key="1" />
    <Global key="2" />
  </>
);
